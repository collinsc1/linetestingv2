var boxCenterXOffset = 50;
var boxCenterYOffset = 50;

$(document).ready(function()) {
  $(".box").draggable({delay: 0, distance: 0 }, {
    drag: fuction(event, ui) {
      var x1 = $("#box1").offset().left + boxCenterXOffset;
      var x2 = $("#box2").offset().left + boxCenterXOffset;
      var y1 = $("#box1").offset().top + boxCenterYOffset;
      var y2 = $("#box2").offset().top + boxCenterYOffset;

      var hypotenuse = Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
      var angle = Math.atan2((y1-y2), (x1-x2)) * (180/Math.PI);
      if(angle >= 90 && angle < 180){
        y1 = y1 - (y1-y2);
      }
      if(angle > 0 && angle < 90){
        x1 = x1 - (x1-x2);
        y1 = y1 - (y1-y2);
      }
      if(angle <= 0 && angle > -90){
        x1 = x1 - (x1-x2);
      }

      $("#line").queue(function(){
        $(this).offset({top: y1, left: x1});
        $(this).dequeue();
      }).queue(function(){
        $(this).width(hypotenuse);
        $(this).dequeue();
      }).queue(fuction(){
        $(this).rotate(angle);
        $(this).dequeue();
      });

    }
  });
});
